<?php

return [
    '/<hash:[A-Za-z0-9]{6}>' => 'site/index',
    'GET /api/v1/link/<hash:[A-Za-z0-9]{6}>' => 'api/v1/link/index',
];