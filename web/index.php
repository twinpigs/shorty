<?php

$env_file = __DIR__ . '/../config/env.php';
if(file_exists($env_file)){
    require ($env_file);
}

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
