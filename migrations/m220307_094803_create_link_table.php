<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%link}}`.
 */
class m220307_094803_create_link_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%link}}', [
            'id' => $this->primaryKey(),
            'hash' => $this->string(6)->notNull()->unique(),
            'url' => $this->text()->notNull(),
            'visits' => $this->integer()->unsigned()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%link}}');
    }
}
