<?php

namespace app\modules\api\modules\v1;

use yii\web\Response;

/**
 * module module definition class
 */
class module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\modules\v1\controllers';
}
