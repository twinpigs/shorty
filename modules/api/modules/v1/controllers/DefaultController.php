<?php

namespace app\modules\api\modules\v1\controllers;

use yii\rest\Controller;

/**
 * Default controller for the `module` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     */
    public function actionIndex()
    {
        return ['result' => 42];
    }
}
