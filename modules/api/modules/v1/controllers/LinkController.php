<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Link;
use app\modules\api\modules\v1\services\LinkService;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class LinkController extends Controller
{

    protected function verbs()
    {
        return [
            'index' => ['GET'],
            'create' => ['POST'],
        ];
    }

    public function actionCreate(): string
    {
        return LinkService::create(\Yii::$app->request->post('url'))->hash;
    }

    public function actionIndex($hash): array
    {

        return $this->findModel($hash)->toArray(['url', 'visits']);
    }

    private function findModel($hash): Link
    {
        if(!($link = Link::findOne(['hash' => $hash]))) {

            throw new NotFoundHttpException();
        }

        return $link;
    }

}
