<?php

namespace app\modules\api\modules\v1\services;

use app\models\Link;
use yii\base\ErrorException;

class LinkService
{
    public static function create($url)
    {
        $link = new Link();
        $link->url = $url;
        if ($link->save()) {

            return $link;
        }

        throw new ErrorException();
    }
}