<?php

namespace app\models;

use yii\base\ErrorException;

/**
 * This is the model class for table "link".
 *
 * @property int $id
 * @property string $hash
 * @property string $url
 * @property int $visits
 */
class Link extends \yii\db\ActiveRecord
{
    const ATTEMPTS_LIMIT = 100;
    protected static $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Hash',
            'url' => 'Url',
            'visits' => 'Visits',
        ];
    }

    private function generateRandomString($length = 6): string
    {
        $sets = explode('|', static::$chars);
        $all = '';
        $random_string = '';
        foreach($sets as $set){
            $random_string .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++){
            $random_string .= $all[array_rand($all)];
        }
        $random_string = str_shuffle($random_string);

        return $random_string;
    }

    private function generateHash(): string
    {
        $hash = $this->generateRandomString();
        $counter = 0;
        while ($counter < static::ATTEMPTS_LIMIT && Link::find()->where(['hash' => $hash])->exists()) {
            $counter++;
            $hash = $this->generateRandomString();
        }

        if($counter >= static::ATTEMPTS_LIMIT) {

            throw new ErrorException();
        }

        return $hash;
    }

    public function touch()
    {
        \Yii::$app->db->createCommand('UPDATE link SET visits = visits + 1 WHERE id = :linkId',
        [
            ':linkId' => $this->id,
        ])->execute();
    }

    public function beforeSave($insert)
    {
        if($insert) {
            $this->hash = $this->generateHash();
        }

        return parent::beforeSave($insert);
    }
}
