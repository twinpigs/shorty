<?php

namespace app\controllers;

use app\models\Link;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($hash = null)
    {
        $result = $this->render('index');
        if($hash) {
            $link = $this->findModel($hash);
            $link->touch();
            $result = $this->redirect($link->url);
        }

        return $result;
    }

    private function findModel($hash) {
        if(!($model = Link::findOne(['hash' => $hash]))) {

            throw new NotFoundHttpException();
        }

        return $model;
    }
}
